(TeX-add-style-hook "ref"
 (lambda ()
    (LaTeX-add-bibitems
     "cartan2012theory"
     "cartan:ru"
     "penrose1987spinors"
     "penrose1987spinors:ru")))

